# NoQuitHere
This plugin changes the player's respawn point based on the last world they disconnected from.

## Config
Go to the config file to set "noquit" worlds, and the respawn world and location.
<pre>noquit-worlds:</pre> is a list of world names. If a player disconnects in any of these worlds, the next time they join the server, they will spawn at the world and location configured under <pre>respawn:</pre>

## Permissions
If a player has the permission <pre>nqh.ignore</pre> the plugin will ignore them, and they will always spawn exactly where they disconnected.