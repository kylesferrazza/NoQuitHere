package io.github.tubakyle.noquithere;

import java.util.List;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class NoQuitHere extends JavaPlugin implements Listener {
	String ver = "v1.0";
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		saveDefaultConfig();
		if (!(ver.equals(getConfig().getString("ver")))) {
			//put stuff in here when config format changes
			//this will be run when config ver doesn't match ver above.
		}
	}
	@EventHandler
	void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (p.hasPermission("nqh.ignore")) {
			return;
		}
		FileConfiguration cfg = this.getConfig();
		List<String> worlds = cfg.getStringList("noquit-worlds");
		if (worlds.contains(p.getWorld().getName())) {
			String path = "players." + p.getName();
			cfg.set(path, true);
			this.saveConfig();
		}
	}
	@EventHandler
	void onJoin(PlayerJoinEvent e) {
		this.reloadConfig();
		FileConfiguration cfg = this.getConfig();
		Player p = e.getPlayer();
		if(cfg.isSet("players." + p.getName())) {
			String path = "players." + p.getName();
			if(cfg.getBoolean(path)) {
				World w = getServer().getWorld(cfg.getString("respawn.world-name"));
				Double x = cfg.getDouble("respawn.x");
				Double y = cfg.getDouble("respawn.y");
				Double z = cfg.getDouble("respawn.z");
				Location l = new Location(w, x, y, z);
				p.teleport(l);
				cfg.set("players." + p.getName(), 0);
				this.saveConfig();
			}
		}
	}
}
